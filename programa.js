let mymap = L.map('mapid');
mymap.setView([4.627447191800487,-74.1682505607605], 16);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
miProveedor.addTo(mymap);
var greenIcon = L.icon({
    iconUrl : 'leaf-green.png' ,
    shadowUrl: 'leaf-shadow.png',
    
    iconSize :    [38, 95],
    shadowSize:   [50, 64],
    iconAnchor:   [22, 94],
    shadowAnchor: [4, 62], 
    popupAnchor:  [-3, -76]
})
var polygon = L.polygon([
    [ 4.62655692708281,  -74.16802793741226],
    [ 4.6268322943673175,  -74.1675129532814],
    [ 4.627043498328498,-74.16762292385101],
    [4.626762784189102,-74.16815400123596]
]).addTo(mymap);
let lamatica = L.marker([4.626965967767793, -74.1675317287445],{icon: greenIcon} )
lamatica.addTo(mymap)
lamatica.bindPopup("aqui esta la matica").openPopup()
let miTitulo=document.getElementById("titulo");
miTitulo.textContent=laplantica.features[0].properties.popupContent
let miDescripcion1=document.getElementById("description1");
miDescripcion1.textContent=laplantica.features[0].properties.description1
let miDescripcion2=document.getElementById("description2");
miDescripcion2.textContent=laplantica.features[0].properties.description2
let miDescripcion3=document.getElementById("description3");
miDescripcion3.textContent=laplantica.features[0].properties.description3
let inicio=document.getElementById("start");
inicio.textContent=laplantica.features[0].properties.start
let miDescripcion4=document.getElementById("description4");
miDescripcion4.textContent=laplantica.features[0].properties.description4
let miDescripciongeneral=document.getElementById("generaldescription");
miDescripciongeneral.textContent=laplantica.features[0].properties.generaldescription
let miTitulo2=document.getElementById("tittle");
miTitulo2.textContent=laplantica.features[0].properties.tittle

 
