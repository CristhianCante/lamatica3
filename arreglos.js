let laplantica={
        "type": "FeatureCollection",
        "features": [
             {
            "type": "Feature",
            "properties": {
                "generaldescription":"En esta pagina se podra evidenciar el desarrollo de una planta de frijol, a la cual se le dio lugar hace poco mas de un mes y que ya ha perecido, a pesar de que esta era regada tres veces a la semana:",
              "popupContent":"Mi planta de frijol",
              "tittle":"Caracteristicas generales",
              "ilustration":"IMG_20201221_140853057.jpg",
              "start":"2020/12/18(fecha de inicio)",
              "ilustration1":"IMG_20210114_203044911.jpg",
              "description1":"la planta mide 15 centimetros",
              "ilustrtion2":"IMG_20210130_221249986.jpg",
"description2":"esta tuvo un tiempo de vida de dos meses",
"ilustration3":"captura1.png",
"description3":"esta es la ubicacion de la planta:",
"ilustration4":"IMG_20210120_210419483.jpg",
"description4":"posee 7 hojas"
            },
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      -74.16802793741226,
                      4.62655692708281
                    ],
                    [
                      -74.1675129532814,
                      4.6268322943673175
                    ],
                    [
                      -74.16762292385101,
                      4.627043498328498
                    ],
                    [
                      -74.16815400123596,
                      4.626762784189102
                    ],
                    [
                      -74.16802793741226,
                      4.62655692708281
                    ]
                  ]
                ]
              },
              
        
        }
        ]
    }